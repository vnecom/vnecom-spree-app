var VALID_DEPLOY_TARGETS = [ //update these to match what you call your deployment targets
  'dev',
  'qa',
  'prod'
];

module.exports = function(deployTarget) {
  var ENV = {
    build: {},
    redis: {
      allowOverwrite: true,
      keyPrefix: 'frontend:index'
    },
    s3: {
      prefix: 'development'
    }
  };
  if (VALID_DEPLOY_TARGETS.indexOf(deployTarget) === -1) {
    throw new Error('Invalid deployTarget ' + deployTarget);
  }

  if (deployTarget === 'dev') {
    ENV.build.environment = 'development';
    ENV.redis.url = process.env.REDIS_URL || 'redis://0.0.0.0:6379/';
    ENV.plugins = ['build', 'redis']; // only care about deploying index.html into redis in dev
  }

  if (deployTarget === 'qa' || deployTarget === 'prod') {
    ENV.build.environment = 'production';
    ENV.s3.accessKeyId = process.env.AWS_KEY;
    
    ENV.s3.secretAccessKey=  process.env.AWS_SECRET;
    ENV.s3.bucket = process.env.AWS_BUCKET;
    ENV.s3.region = process.env.AWS_REGION
  }

  if (deployTarget === 'qa') {
    ENV.redis.url = process.env.QA_REDIS_URL;
  }

  if (deployTarget === 'prod') {
    //ENV.redis.url = 'redis://127.0.0.1:6379/';
      ENV.redis.url = process.env.REDIS_URL
  }

  return ENV;

}
