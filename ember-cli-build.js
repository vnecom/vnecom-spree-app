/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');


module.exports = function(defaults) {
  var app = new EmberApp({
    // Add options here
		
    fingerprint:{
    enabled:true,
		prepend:"http://d2cxx1zdxve54p.cloudfront.net/development/"
		},
		extensions: ['js', 'css', 'png', 'jpg'],
		minifyCSS: {enabled: true},
		minifyJS: {enabled: true}

 
  });

 


var files = [
  'bower_components/bootstrap/dist/css/bootstrap.min.css',
	'bower_components/bootstrap/dist/js/bootstrap.min.js',
	'vendor/css/unify/app.css',

	'vendor/css/unify/blocks.css',
	'vendor/css/unify/onestyle.css',
	'vendor/css/unify/footerv7.css',

	'vendor/css/unify/appstyle.css',
	'vendor/css/libs/animate.css',
	'vendor/css/unify/lineicons.css',
	 'vendor/css/unify/paceflash.css',
	  'vendor/css/libs/owl-carousel2/assets/owl.carousel.css',
	 'vendor/css/libs/owl-carousel/owl-carousel/owl.carousel.css',
	 'vendor/css/libs/cubeportfolio/css/cubeportfolio.min.css',
	 'vendor/css/libs/cubeportfolio/custom/custom-cubeportfolio.css',
	 'vendor/css/unify/shhos.css',
	 'vendor/css/custom/app.style.css',
	 'vendor/css/custom/shippy-color.css',
	 	 'vendor/css/custom/form.css',
		 'vendor/css/custom/custom.css'

 ];
files.forEach(function(f){
    merge(f,'assets');
});






var plugins = [
"vendor/plugins/smoothScroll.js",
"vendor/plugins/jquery.easing.min.js",
"vendor/plugins/owl-carousel2/owl.carousel.min.js",
"vendor/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js",
"vendor/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js",
"vendor/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js",
"vendor/js/paceLoader.js",
"vendor/js/cube-portfolio3.js",
"vendor/plugins/modernizr.js",
"vendor/plugins/owl-carousel2/owl.carousel.js"
];
plugins.forEach(function(f){
	merge(f,'assets');
});
function merge(fileName,src){
  app.import(fileName,{
    destDir: src
  });

}




  // css from templates



  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  return app.toTree();
};
