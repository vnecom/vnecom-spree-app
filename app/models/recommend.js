import DS from 'ember-data';

export default DS.Model.extend({
  quote_id: DS.attr(), 
  email: DS.attr(),
  first_name: DS.attr(),
  last_name: DS.attr(),
  address: DS.attr(),
	phone_number: DS.attr()
});
