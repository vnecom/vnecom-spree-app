import DS from 'ember-data';
import Ember from 'ember';
var category = DS.Model.extend({
  name: DS.attr(),
  products: DS.hasMany('product', { async: true}),
  filter_id: Ember.computed('id', function() {
    return '.' + this.get('id');
  })
});



category.reopenClass({
  FIXTURES: [{
    id: 1,
    name: "Sách",
    products: [
      1,
      4,
      5,
      6,
      7,
      8
    ],
    quotes: []
  }, {
    id: 2,
    name: "Thời trang",
    products: [
      2,
      3
    ],

    quotes: []
  }, {
    id: 3,
    name: "Đồ công nghệ",
    products: [
      9,
      10
    ],

    quotes: []
  }, {
    id: 4,
    name: "Đồ cho bé",
    products: [
      11,
      12
    ]
  }],


    quotes: []

});

export default category;
