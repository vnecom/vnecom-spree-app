import DS from 'ember-data';

export default DS.Model.extend({
 product_link: DS.attr(),
 original_price: DS.attr(),
 category_id: DS.attr(),
 suggested_price: DS.attr()
});
