
import DS from 'ember-data';
import Ember from 'ember';
var product = DS.Model.extend({
  name: DS.attr(),
	brand: DS.attr(),
	brand_link:DS.attr(),
	author: DS.attr(),
  short_description: DS.attr(),
  total_price: DS.attr(),
	service_fee:DS.attr(),
	tax_info:DS.attr(),
	carousel_number: DS.attr(),
	saving: DS.attr(),
	original_link: DS.attr(),
	feature_title: DS.attr(),
	feature_h1: DS.attr(),
	feature_1_description: DS.attr(),
	feature_h2: DS.attr(),
	feature_2_description: DS.attr(),
	expire_by:DS.attr(),
	short_name: DS.attr(),
	option_keys: DS.attr(),
	option_values: DS.attr(),
	suggested_products: DS.attr(),
  category: DS.belongsTo('category' ,{ async: true }),

	slug: Ember.computed("id","short_name", function(){

		var name = this.get('id') + "-" + this.get("short_name");
		return name;
	}),
	suggest_link: Ember.computed('id','short_name',function(){
		var l = window.location.protocol + "//"+ window.location.host+"/products/"+ this.get('id')+"-"+this.get("short_name");
		return l;
	}),

	producer: Ember.computed('brand','author', function(){
		return this.get('brand').length>0 ? this.get('brand'):this.get('author');
	}),
	filter_id: Ember.computed('category',function(){
		// console.log(this.get("category.name"));
		return "cbp-item "+ this.get('category.id');
	}),
	expire_date: Ember.computed('expire_by',function(){
		// console.log(this.get('expire_by'));
		var date= new Date(this.get('expire_by'));

		// var new_date = e_d.toLocaleDateString().split("/");
		return [date.getDate(), date.getMonth()+1, date.getFullYear()].join('/');
	}),
	days_left: Ember.computed('expire_by',function(){
		var current = new Date(Date.now());
		var due = new Date(this.get('expire_by'));
		var l = parseInt((due - current)*1.15740741e-8);

		return l<0? 0:l;

	}),
	img: Ember.computed('short_name','carousel_number',function(){
		var name = this.get("short_name");
		var url = "http://d2cxx1zdxve54p.cloudfront.net/media/" + name + "/";
		var main_img = url+name+"-shop.jpg";
		var feature_img = url+name+"-feature.jpg";
		var top_img = url+name+"-top.png";
		var carousel_imgs = [];
	  var temp_img = "";
		var share_img = url+name+"-share.jpg";
		var suggest_img = url+name+"-suggest.jpg";

		var num_c = this.get('carousel_number');
		if(num_c === 0 ){
			temp_img = url+name+"-main.jpg";
			carousel_imgs.push(temp_img);
		} else {
			for (var i =0; i< num_c;i++){
				temp_img = url+"main-carousel/"+name+"-main-"+i+".jpg";
				carousel_imgs.push(temp_img);
			}
		}
		// console.log(carousel_imgs);
		return {
			main_img: main_img,
			feature_img: feature_img,
			top_img: top_img,
			carousel_imgs: carousel_imgs,
			share_img: share_img,
			suggest_img: suggest_img
		};
	})
});

product.reopenClass({

FIXTURES: [{
id: 1,
name: "Sách The Tipping Point (Bìa mềm)",
short_description: "The Tipping Point viết về sức ảnh hưởng của những điều tuy rất nhỏ nhưng lại là điểm trọng yếu có thể mở đầu cho cả một hiện tượng, một sự đột phá khác biệt, và thậm chí là cả một hiệu ứng dây chuyền.",
saving: "330,000",
original_price: null,
total_price: "509,000",
tax_info: "Tại Việt Nam, đối với sản phẩm sách,thuế nhập khẩu là 0% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Cuốn sách bạn không thể bỏ qua",
feature_h1: "Một trong những sách bán chạy nhất của quốc gia tại Mỹ",
feature_h2: "Tầm ảnh hưởng rộng trong ngành Marketing và Quảng Cáo",
service_fee: null,
brand: "",
author: "Malcolm Gladwell",
brand_link: "",
carousel_number: 0,
feature_1_description: "Hơn 1.7 triệu bản đã được bán chỉ trong 6 năm đầu tiên. Được hơn 400 nghìn người yêu thích trên Goodreads",
feature_2_description: "Tác động vào cách nhìn của hàng nghìn độc giả về việc bán hàng và lan truyền ý tưởng. ",
expire_by: "Sun March 20 2016 23:59:59 GMT+0700",
short_name: "sach-tipping-point",
option_keys: null,
option_values: [ ],
suggested_products: [
9,
7,
3,
2
],
original_link: "http://www.amazon.com/The-Tipping-Point-Little-Difference/dp/0316346624",
category: 1
},
{
id: 2,
name: "Sách Official SAT Study Guide 2016",
short_description: "Sách hướng dẫn học SAT chính thức được viết bởi những người soạn đề thi tại College Board. Nội dung bao gồm cấu trúc đề thi mới sắp được áp dụng kể từ kì thi của tháng 3 năm 2016 trở đi.",
saving: "370,000",
original_price: null,
total_price: "559,000",
tax_info: "Tại Việt Nam, đối với sản phẩm sách,thuế nhập khẩu là 0% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Tài liệu quan trọng cho kì thi SAT mới bắt đầu từ tháng 3/2016",
feature_h1: "Thích hợp cho việc tự học và chuẩn bị cho kì thi",
feature_h2: "Hơn 300 trang hướng dẫn chiến lược học cho kì thi và cách giải các câu hỏi trong đề",
service_fee: null,
brand: "College Board",
author: "",
brand_link: "https://www.collegeboard.org/",
carousel_number: 0,
feature_1_description: "Bao gồm 4 đề thi thử và nhiều hướng dẫn phương pháp học cụ thể",
feature_2_description: "Các đáp án được giải thích cụ thể bởi những người soạn đề thi chính thức. Có các mục Review sau mỗi phần, giúp học sinh có thể củng cố những kiến thức đã học được một cách hiệu quả",
expire_by: "Sun March 20 2016 23:59:59 GMT+0701",
short_name: "sach-official-sat-study-guide-2016",
option_keys: null,
option_values: [ ],
suggested_products: [
3,
12,
1,
11
],
original_link: "http://www.amazon.com/Official-Study-Guide-2016-Edition/dp/1457304309/ref=cm_cr_pr_product_top?ie=UTF8",
category: 1
},
{
id: 3,
name: "Sách Official Guide to the TOEFL",
short_description: "Sách luyện thi chứng chỉ TOEFL được viết bởi những người soạn đề thi chính thức tại ETS.",
saving: "360,000",
original_price: null,
total_price: "759,000",
tax_info: "Tại Việt Nam, đối với sản phẩm sách,thuế nhập khẩu là 0% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Sự chuẩn bị chu đáo nhất cho kì thi TOEFL",
feature_h1: "Tài liệu luyện thi TOEFL uy tín nhất",
feature_h2: "Luyện tập thực tế",
service_fee: null,
brand: "Educational Testing Service (ETS)",
author: "",
brand_link: "https://www.ets.org/",
carousel_number: 0,
feature_1_description: "Bao gồm những câu hỏi đã được dùng trong đề thi thật trước đây. Cách trả lời câu hỏi và phương pháp làm những bài thi nghe nói được hướng dẫn bởi những người soạn đề",
feature_2_description: "Một CD-ROM với 3 bài thi thử TOEFL iBT giúp học sinh có thể thử sức với những bài thi đầy trên máy tính như thi thật. Sau những phần nghe và đọc trong mỗi bài thi thử, học sinh có thể biết được ngay kết quả của những phần này khi kết thúc bài thi.",
expire_by: "Sun March 20 2016 23:59:59 GMT+0702",
short_name: "sach-official-guide-to-the-toefl",
option_keys: null,
option_values: [ ],
suggested_products: [
2,
1,
11,
8
],
original_link: "http://www.amazon.com/dp/0071766588/ref=wl_it_dp_o_pC_nS_ttl?_encoding=UTF8&colid=I5PWIC2EHJVC&coliid=IURS4BBKX40XP",
category: 1
},
{
id: 4,
name: "Ví Kate Spade New York - Stacy",
short_description: "Tự tin ra phố với ví Stacy của Kate Spade. Thiết kế theo phong cách châu Âu, đơn giản thanh lịch với chất liệu da dầy và chắc chắn với những đường may thanh gọn.",
saving: "400,000",
original_price: null,
total_price: "3,299,000",
tax_info: "Tại Việt Nam, đối với sản phẩm thời trang,thuế nhập khẩu là 20% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Thương hiệu Mỹ, phong cách châu Âu",
feature_h1: "Thiết kế tao nhã và sang trọng",
feature_h2: "Tiện lợi và dễ sử dụng",
service_fee: null,
brand: "Kate Spade New York",
author: "",
brand_link: "https://www.katespade.com/",
carousel_number: 7,
feature_1_description: "Biểu tượng thương hiệu của Kate Spade New York được chạm nổi. Các cấu trúc thiết kế bằng kim loại được mạ kĩ lưỡng bởi 14 karat vàng.",
feature_2_description: "12 ngăn để thẻ tín dụng, các loại thẻ thành viên và card visit. 2 ngăn lớn rộng rãi để chứa tiền mặt và hóa đơn.",
expire_by: "Sun March 20 2016 23:59:59 GMT+0703",
short_name: "vi-kate-spade-new-york",
option_keys: "Màu",
option_values: [
"Rough pink",
" Cherry liqueur/Ballerina",
" Clocktower/Flo geranium",
" Crisp linen",
" Flo geranium",
" Grace blue",
" Stone ice rough pink"
],
suggested_products: [
4,
6,
14,
13
],
original_link: "https://www.katespade.com/products/cedar-street-stacy/PWRU3905.html?cgid=ks-accessories-wallets&dwvar_PWRU3905_size=UNS&dwvar_PWRU3905_color=296#start=18&cgid=ks-accessories-wallets",
category: 2
},
{
id: 5,
name: "Ví Fossil - Sydney Zip Phone",
short_description: "Cuộc sống bận rộn sẽ tiện lợi hơn với chiếc ví Sydney Zip Phone có thể bỏ vừa chiếc điện thoại thông minh của bạn. Có thể chứa vừa vặn những điện thoại có kích thước lớn như iPhone 6/6s và Samsung Galaxy 6S.",
saving: "410,000",
original_price: null,
total_price: "2,119,000",
tax_info: "Tại Việt Nam, đối với sản phẩm thời trang,thuế nhập khẩu là 20% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Phong cách Vintage nhưng vẫn tân thời",
feature_h1: "Kiểu dáng quý phái và hiện đại",
feature_h2: "Tiện lợi ở mọi nơi",
service_fee: null,
brand: "Fossil",
author: "",
brand_link: "https://www.fossil.com/us/en.html",
carousel_number: 4,
feature_1_description: "Chất liệu 100% da cao cấp, mềm và luôn bền qua thời gian. Gia công tỉ mỉ với đường may ngọn gàng hoàn hảo.",
feature_2_description: "Có dây đeo tay gắn kèm, 3 ngăn chứa thẻ, 2 ngăn lớn chứa tiền mặt,1 ngăn kéo ngoài và 1 ngăn kéo trong",
expire_by: "Sun March 20 2016 23:59:59 GMT+0704",
short_name: "vi-fossil-sydney-zip-phone",
option_keys: null,
option_values: [ ],
suggested_products: [
5,
7,
13,
14
],
original_link: "https://www.fossil.com/us/en/wallets/womens-wallets/view-all/sydney-zip-phone-wallet-sku-sl6687200c.html",
category: 2
},
{
id: 6,
name: "Sách Usborne - Listen and Learn First English Words",
short_description: "Sách dạy trẻ nhỏ bắt đầu học tiếng Anh một cách sáng tạo và sống động. Sách có độ tương tác cao, giúp trẻ nghe cách phát âm của những từ tiếng Anh đơn giản.",
saving: "350,000",
original_price: null,
total_price: "699,000",
tax_info: "Tại Việt Nam, đối với sản phẩm sách,thuế nhập khẩu là 0% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Cho trẻ con học Anh Văn vừa vui vừa hiệu quả",
feature_h1: "Nội dung đa dạng và tương tác cao",
feature_h2: "Chất liệu và thiết kế phù hợp với trẻ nhỏ",
service_fee: null,
brand: "Nhà xuất bản Usborne",
author: "",
brand_link: "https://new.myubam.com/",
carousel_number: 0,
feature_1_description: "Từ vựng được trình bày theo 8 chủ đề khác nhau. Gồm các thẻ từ vựng bọc nhựa. Trẻ có thể gắn thẻ vào trang sách, ấn nút và nghe từ mới được phát âm rõ ràng.",
feature_2_description: "Giấy dầy và bìa cứng. Dù trẻ có nghịch phá thì cũng không mau hỏng. Hình ảnh và màu sắc trình bày rõ ràng và rất sinh động, kích thích trẻ học và khám phá từ mới.",
expire_by: "Sun March 20 2016 23:59:59 GMT+0705",
short_name: "sach-usborne-listen-and-learn-first-english-words",
option_keys: null,
option_values: [ ],
suggested_products: [
13,
14,
4,
5
],
original_link: "http://b1306.myubam.com/p/4497/listen-and-learn-first-english-words",
category: 1
},
{
id: 7,
name: "Sách Artbook Zen Pencils",
short_description: "Truyện tranh tiếng Anh truyền cảm hứng của họa sĩ người Úc, Gavin Aung Than. ",
saving: "380,000",
original_price: null,
total_price: "488,000",
tax_info: "Tại Việt Nam, đối với sản phẩm sách,thuế nhập khẩu là 0% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Nhẹ nhàng và sâu sắc",
feature_h1: "Ít lời nhưng giàu ý nghĩa",
feature_h2: "Tính giáo dục cao",
service_fee: null,
brand: "",
author: "Gavin Aung Than",
brand_link: "http://zenpencils.com/",
carousel_number: 0,
feature_1_description: "Các mẫu truyện tranh nhỏ được dựa trên những câu nói nổi tiếng của các danh nhân và những người thành công từ khắp thế giới. Những câu nói đầy ý nghĩ của những danh nhân được minh họa lại bằng những mẩu truyện xúc tích, thẩm mỹ cao, và gần gũi. ",
feature_2_description: "Với hình ảnh sống động và mang đậm tính nhân văn, Zen đã được sử dụng làm sách dạy học chính thức ở một trường tiểu học tại Ấn Độ. Ở nhiều nơi khác, nội dung trong Zen Pencils được trình bày và chia sẻ trong trường học",
expire_by: "Sun March 20 2016 23:59:59 GMT+0706",
short_name: "sach-artbook-zen-pencils",
option_keys: null,
option_values: [ ],
suggested_products: [
9,
1,
10,
8
],
original_link: "http://www.amazon.com/dp/1449457959/ref=wl_it_dp_o_pC_nS_ttl?_encoding=UTF8&colid=I5PWIC2EHJVC&coliid=I1KHT5CVSPQWG8",
category: 1
},
{
id: 8,
name: "Sách Humans of New York",
short_description: "Bộ sách nhiếp ảnh nổi tiếng đình đám của Brandon Stanton với những góc nhìn đa dạng về thành phố New York của Mỹ. Sau mỗi tấm hình và mỗi khoảnh khắc được lưu lại là những câu thoại hoặc những mẫu chuyện ngắn mà gần gũi, đi vào lòng người và đáng suy ngẫm.",
saving: "260,000",
original_price: null,
total_price: "580,000",
tax_info: "Tại Việt Nam, đối với sản phẩm sách,thuế nhập khẩu là 0% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Tinh thần của New York",
feature_h1: "Tự do và đa dạng",
feature_h2: "Đầy cảm hứng",
service_fee: null,
brand: "",
author: "Brandon Stanton",
brand_link: "http://www.humansofnewyork.com/",
carousel_number: 0,
feature_1_description: "400 ảnh với nội dung sống động và đa dạng. Mỗi hình ảnh thể hiện một cái nhìn riêng và những câu chuyện khác nhau tại New York.",
feature_2_description: "Nội dung hình ảnh và câu chuyện được hàng triệu người hâm mộ trên thế giới theo dõi. Những thú vị bất ngờ khi nhìn vào những góc cạnh khác nhau của cuộc sống",
expire_by: "Sun March 20 2016 23:59:59 GMT+0707",
short_name: "sach-humans-of-new-york",
option_keys: null,
option_values: [ ],
suggested_products: [
7,
10,
1,
13
],
original_link: "http://www.amazon.com/dp/1250038820/ref=wl_it_dp_o_pC_nS_ttl?_encoding=UTF8&colid=I5PWIC2EHJVC&coliid=I1OK7ZHDPZSXTI",
category: 1
},
{
id: 9,
name: "Sách The Lean Startup ",
short_description: "Quyển sách về mô hình khởi nghiệp Lean không thể thiếu đối với các nhà sáng lập kinh doanh.",
saving: "360,000",
original_price: null,
total_price: "609,000",
tax_info: "Tại Việt Nam, đối với sản phẩm sách,thuế nhập khẩu là 0% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: " Mô hình khởi nghiệp hiệu quả ",
feature_h1: "Cuốn sách dành cho",
feature_h2: "Được áp dụng phổ biến",
service_fee: null,
brand: "",
author: "Eric Ries",
brand_link: "http://theleanstartup.com/",
carousel_number: 0,
feature_1_description: "Những người sáng lập công ty khởi nghiệp. Bất cứ người làm kinh doanh nào muốn tạo nên sản phẩm mới cho doanh nghiệp hiện có. Những người lãnh đạo muốn làm mới lại sản phẩm trong những công ty tập đoàn.",
feature_2_description: "Dropbox, Groupon, Wealthfront, IMVU, Grockit và nhiều doanh nghiệp khác đã áp dụng mô hình Lean và đạt được rất nhiều thành công rất đáng kể.",
expire_by: "Sun March 20 2016 23:59:59 GMT+0708",
short_name: "sach-the-lean-startup",
option_keys: null,
option_values: [ ],
suggested_products: [
1,
8,
13,
7
],
original_link: "http://www.amazon.com/dp/0307887898/ref=wl_it_dp_o_pC_nS_ttl?_encoding=UTF8&colid=I5PWIC2EHJVC&coliid=IDFAWC4SCJ7LP",
category: 1
},
{
id: 10,
name: "Sách The Art of Spirited Away",
short_description: "The Art of Spirited Away là một bộ sách ảnh gồm 240 trang concept art và những bản vẽ từ vẽ tay đến vẽ trên máy của Miyazaki.",
saving: "380,000",
original_price: null,
total_price: "629,000",
tax_info: "Tại Việt Nam, đối với sản phẩm sách,thuế nhập khẩu là 0% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Thế giới nghệ thuật sắc sảo đằng sau một bộ phim hoạt hình",
feature_h1: "Không chỉ có hình ảnh",
feature_h2: "Cái nhìn đa chiều",
service_fee: null,
brand: "Studio Ghibli",
author: "",
brand_link: "",
carousel_number: 7,
feature_1_description: "Bên cạnh những phác họa nhân vật còn là những câu chuyện và suy nghĩ của tác giả khi ông vẽ từng chi tiết.",
feature_2_description: "Cuốn sách cho độc giả chiêm ngưỡng và tìm hiểu từ những nét vẽ thủ công nhất cho đến những bản vẽ đồ hoạt công nghệ cao tinh tế nhất trên máy tính.",
expire_by: "Sun March 20 2016 23:59:59 GMT+0709",
short_name: "sach-artbook-spirited-away",
option_keys: null,
option_values: [ ],
suggested_products: [
8,
7,
13,
11
],
original_link: "http://www.amazon.com/Art-Spirited-Away-Hayao-Miyazaki/dp/1569317771/ref=sr_1_1?ie=UTF8&qid=1456638339&sr=8-1&keywords=art+of+spirited+away",
category: 1
},
{
id: 11,
name: "Pin sạc dự phòng Anker 10000mAh",
short_description: "Pin xạc dự phòng cho các loại điện thoại và thiết công nghệ với 18 tháng bảo hành",
saving: "580,000",
original_price: null,
total_price: "699,000",
tax_info: "Tại Việt Nam, đối với sản phẩm điện tử,thuế nhập khẩu là 0% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Nhỏ. Nhẹ. Bền.",
feature_h1: "Kích thước nhỏ bằng một chiếc thẻ tín dụng",
feature_h2: "Yên tâm sử dụng trong mọi trường hợp",
service_fee: null,
brand: "Anker",
author: "",
brand_link: "https://www.anker.com/",
carousel_number: 4,
feature_1_description: "Tuy hình dáng nhỏ một cách ngạc nhiên, pin xạc Anker vẫn có thể 3 lần liên tục xạc đầy pin cho điện thoại",
feature_2_description: "Pin xạc Anker được thiết kế với khả năng chống được sốc và chịu được nhiệt cực tốt. Bền lâu và an toàn cho người sử dụng.",
expire_by: "Sun March 20 2016 23:59:59 GMT+0710",
short_name: "sac-du-phong-anker",
option_keys: null,
option_values: [ ],
suggested_products: [
12,
14,
1,
2
],
original_link: "http://www.amazon.com/dp/B0194WDVHI/ref=wl_it_dp_o_pC_nS_ttl?_encoding=UTF8&colid=I5PWIC2EHJVC&coliid=I11MMCWWQ7XO69&psc=1",
category: 3
},
{
id: 12,
name: "Loa nghe nhạc không dây JBL Bluetooth",
short_description: "Loa nghe nhạc không dây với kết nối bluetooth. Kiểu dáng nhỏ với chất lượng âm thanh cao.",
saving: "530,000",
original_price: null,
total_price: "1,039,000",
tax_info: "Tại Việt Nam, đối với sản phẩm điện tử,thuế nhập khẩu là 0% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Vang dội. Gọn gàng. Hiện đại.",
feature_h1: "Âm thanh chất lượng cao",
feature_h2: "Gọn gàng và tiện lợi",
service_fee: null,
brand: "JBL",
author: "",
brand_link: "http://www.jbl.com/",
carousel_number: 5,
feature_1_description: "Âm thanh vang to và tốt như những loại loa lớn. Nghe rõ được các thể loại bass và những âm thanh cao độ",
feature_2_description: "Bỏ vừa túi sách. Dễ mang theo. Sử dụng bluetooth không cần dây. Nghe nhạc liên tục trong vòng 5 tiếng",
expire_by: "Sun March 20 2016 23:59:59 GMT+0711",
short_name: "loa-nghe-nhac-khong-day-jbl",
option_keys: "Màu",
option_values: [
"Đỏ",
" Đen"
],
suggested_products: [
11,
8,
3,
4
],
original_link: "http://www.amazon.com/dp/B009AYLDSU/ref=wl_it_dp_o_pC_nS_ttl?_encoding=UTF8&colid=I5PWIC2EHJVC&coliid=I9QBSWZJIOLQY&psc=1",
category: 3
},
{
id: 13,
name: "Rô bốt biến hình Woobots",
short_description: "Mô hình Rô bốt lắp ghép thông minh, có thể biến hình thành nhiền vật thể khác nhau chỉ từ một mẫu thiết kế.",
saving: "580,000",
original_price: null,
total_price: "1,049,000",
tax_info: "Tại Việt Nam, đối với sản phẩm đồ chơi,thuế nhập khẩu là 15% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Đẳng cấp và sáng tạo",
feature_h1: "Thiết kế độc đáo",
feature_h2: "Mô hình sáng tạo",
service_fee: null,
brand: "Bamloff",
author: "",
brand_link: "http://bamlofftoy.com/web/",
carousel_number: 3,
feature_1_description: "100% được làm từ gỗ thật, chất lượng và có độ bền rất cao. Mỗi nhân vật được cấu tạo từ 15 - 20 viên gỗ được cắt tỉa và đo lường với độ chuẩn xác hoàn hảo",
feature_2_description: "Ngoài 2 tạo hình được nhà thiết kế dựng sẵn, mỗi nhân vật đều có thể được biến hóa thành bất kì hình dáng nào khác. Cấu tạo linh động không chỉ kích thích sự sáng tạo cao cho trẻ em mà còn cho cả người lớn.",
expire_by: "Sun March 20 2016 23:59:59 GMT+0712",
short_name: "do-choi-xep-hinh-woobots",
option_keys: null,
option_values: [ ],
suggested_products: [
7,
14,
11,
5
],
original_link: "https://www.indiegogo.com/projects/woobots-transformable-wooden-robot#/",
category: 4
},
{
id: 14,
name: "Máy đo thân nhiệt Innovo",
short_description: "Máy đo thân nhiệt Innovo có thể đo nhiệt độ cho trẻ qua trán hoặc tai một cách nhanh chóng và luôn luôn chính xác. Là dụng cụ đo thân nhiệt uy tín, được chứng nhận bởi FDA, Cục quản lý Thực phẩm và Dược phẩm Hoa Kỳ.",
saving: "500,000",
original_price: null,
total_price: "1,199,000",
tax_info: "Tại Việt Nam, đối với sản phẩm điện tử,thuế nhập khẩu là 0% và thuế GTGT là 10%. Tuy nhiên, với Shippy trọn gói từ A đến Z, bạn không phải lo nghĩ về vấn đề này khi mua sắm nữa.",
feature_title: "Tính năng đặc biệt và tiện lợi",
feature_h1: "Cách dùng đơn giản",
feature_h2: "Thông báo thông minh và tiện lợi",
service_fee: null,
brand: "Innovo",
author: "",
brand_link: "http://innovo-medical.com/",
carousel_number: 4,
feature_1_description: "Thiết kế chỉ với 2 nút bấm dễ sử dụng. Màn hình LED hiển thị số rõ ràng và chính xác.",
feature_2_description: "Máy sẽ tự động phát báo hiệu khi nhiệt độ của trẻ lên quá 37.5 °C. Sử dụng đo thân nhiệt cho trẻ được hàng nghìn lần chỉ với một lần thay pin",
expire_by: "Sun March 20 2016 23:59:59 GMT+0713",
short_name: "may-do-than-nhiet-innovo",
option_keys: null,
option_values: [ ],
suggested_products: [
7,
11,
4,
5
],
original_link: "http://www.amazon.com/dp/B00OY7UY60/ref=wl_it_dp_o_pC_nS_ttl?_encoding=UTF8&colid=I5PWIC2EHJVC&coliid=I1MHIEZABQM8U3&psc=1",
category: 4
}
]
});


export default product;
