import DS from 'ember-data';

export default DS.Model.extend({
			email: DS.attr(),
			first_name: DS.attr(),
			last_name: DS.attr(),
			card_type: DS.attr(),
			province: DS.attr(),
			address: DS.attr(),
			phone_number: DS.attr(),
			product_id: DS.attr(),
			options: DS.attr()
});
