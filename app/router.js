import Ember from 'ember';
// import DS from 'ember-data';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
		this.resource('products');
		this.resource('product',{path:'products/:product_slug'});
    this.route('recommend');
		this.route('ve-shippy');
		this.route('ho-tro-khach-hang');
		this.route('chinh-sach-doi-tra');
		this.route('thankyou');
		this.route('tiet-kiem');

});

export default Router;
