import Ember from 'ember';
import _ from 'underscore';
export default Ember.Controller.extend({
  stepOne: true,
  stepTwo: false,
  stepThree: false,
  lastStep:false,
  quote:{},
  contact:{},
  actions:{
    

    //STEP 1: record quote and generate suggested_price
    setQuote: function(quote_id){
      console.log("Requested!...");
      let generated_quote = this.get('store').findRecord("quote",quote_id);
      this.set("quote",generated_quote);
      //move to step 2 which is show quote info and ask user to proceed or not
      this.set("stepOne",false);
      this.set("stepTwo",true);
    },

    //STEP 2: show quote and ask if user wants to continues
    getContactInfo: function(recommend){
      this.set("stepTwo",false);
      this.set("stepThree",true);
    },
    //STEP 3: save user info and submit request
    submitRecommend: function(){
      var self = this;
      var recParams = this.get("contact");

      /* Create recommend and save */

      let recommend = this.get('store').createRecord('recommend',{
        email:recParams.email,
        first_name: recParams.first_name,
        last_name: recParams.last_name,
        address: recParams.address,
        quote_id: this.get("quote").get("id")
      });

      recommend
        .save()
        .then(function(data){
          console.log(JSON.stringify(data));
          self.set("stepThree",false);
          self.set("lastStep",true);

        })
        .catch(function(err){
          console.log(err);
        });

    }


  }
});
