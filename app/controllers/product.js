import Ember from 'ember';
import _ from "underscore";
export default Ember.Controller.extend({
	suggestions: Ember.computed('model','model.name','model.suggested_products',function(){
		var product = this.model,
			list_suggestion = product.get('suggested_products');
		var result = [];
	   var store = this.store;


			list_suggestion.forEach(function(value){
			 let p = store.findRecord('product',value);
				result.pushObject(p);
				});

		console.log(result);
		return result;

	}),
	form: {},
	success:false,
	error: {status:false,message:""},
	actions:{

		submitRequest: function(obj,product_id){
			function validateEmail(email) {
			    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			    return re.test(email);
				}


				if(!validateEmail(obj.email)){
					this.set("error",{status:true,message:"Vui lòng nhập địa chỉ email hợp lệ."});
				} else if (_.values(obj).length <5){
					this.set("error",{status:true,message:"Vui lòng nhập đủ thông tin:)"});
				} else{
					this.set("error",{status:false,message:"OK"});
				}
				obj.product_id = product_id;
				if(this.get("error").status === false){
					var order = this.store.createRecord("order",obj);
					var self = this;
					order
					.save()
					.then(function(){
						Ember.$("#buy").hide();

						var l = window.location.protocol+"//"+window.location.host+"/thankyou";

						window.location.href =  l;

						console.log("Created!");

					})
					.catch(function(){
					self.set("error",{status:true,message:"Vui lòng kiểm tra lại thông tin:)"});
					Ember.$("#buy").show();

				});
				}


		},
		scrollBack: function(){

			Ember.$('html, body').animate({
	    scrollTop: Ember.$("#ti").offset().top
	}, 2000);

	}
},
didInsertElement:function(){
	this.set("form",{});

}
});
