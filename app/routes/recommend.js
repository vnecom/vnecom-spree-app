import Ember from 'ember';

export default Ember.Route.extend({
  model(){
    return {
      categories: this.store.findAll('category')
    }
  },
	activate:function(){
		var title = "Gửi đơn hàng riêng | Shippy.vn ";
		document.title = title;
		var url = "http://shippy.vn/recommend";
		Ember.$("meta[property='og\\:url']").attr("content", url);
		Ember.$("meta[property='og\\:title']").attr("content", title);
		window.scrollTo(0,0);
	}
});
