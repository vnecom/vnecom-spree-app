import Ember from 'ember';

export default Ember.Route.extend({
	activate:function(){
		var title = "Chính sách đổi trả | Shippy.vn";
		document.title = title;
		var url = "http://shippy.vn/chinh-sach-doi-tra";

		Ember.$("meta[property='og\\:url']").attr("content", url);
		Ember.$("meta[property='og\\:title']").attr("content",title);
		window.scrollTo(0,0);
	}
});
