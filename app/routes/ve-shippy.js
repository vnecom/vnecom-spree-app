import Ember from 'ember';


export default Ember.Route.reopen({
	activate:function(){
		var title = "Giới thiệu Shippy | Shippy.vn";
		document.title = title;
		var url = "http://shippy.vn/ve-shippy";

		Ember.$("meta[property='og\\:url']").attr("content", url);
		Ember.$("meta[property='og\\:title']").attr("content", document.title);

		}
});
