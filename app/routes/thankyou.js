import Ember from 'ember';

export default Ember.Route.extend({
	activate:function(){
		var title = "Cảm ơn bạn đã đặt hàng";
		document.title = title;
		var url = "http://shippy.vn/thankyou";
		Ember.$("meta[property='og\\:url']").attr("content", url);
		Ember.$("meta[property='og\\:title']").attr("content", title);
	}
});
