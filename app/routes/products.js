import Ember from 'ember';

var route = Ember.Route.extend({
  model() {
		return Ember.RSVP.hash({
			products: this.store.findAll('product'),
			categories: this.store.findAll('category')
		});
	},
	activate: function(){
	var title = 'Sản phẩm mới từ Mỹ | Shippy.vn (Trang tất cả sản phẩm)';

	document.title = title;
	var url = "http://shippy.vn/products";
	Ember.$("meta[property='og\\:url']").attr("content", url);
	Ember.$("meta[property='og\\:title']").attr("content", title);
	window.scrollTo(0,0);
}
});



export default route;
