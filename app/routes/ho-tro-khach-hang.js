import Ember from 'ember';

export default Ember.Route.extend({
	activate:function(){
		// document.title= "Hỗ trợ khách hàng | Shippy.vn";
		// var url = "http://shippy.vn/ho-tro-khach-hang";
		// Ember.$("meta[name='og\\:url']").attr("content", url);
		// Ember.$("meta[name='og\\:title']").attr("content", document.title);
		var title = "Hỗ trợ khách hàng | Shippy.vn";
		document.title = title;
		var url = "http://shippy.vn/ho-tro-khach-hang";
		Ember.$("meta[property='og\\:url']").attr("content", url);
		Ember.$("meta[property='og\\:title']").attr("content", title);
	 window.scrollTo(0,0);
  }
});
