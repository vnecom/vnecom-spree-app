import Ember from 'ember';
var route = Ember.Route.extend({
  model: function(params){

    // return this.store.query-record('product',params.product_id);
		console.log("rooute ... "+ params.product_slug);

		var id = parseInt(decodeURIComponent(params.product_slug.split("-")[0]));
		return this.store.findRecord('product', id);
	},
	serialize: function(model) {
    return {
      product_slug: model.get('slug')
    };
  }
});
route.reopen({
	activate: function(){
		window.scrollTo(0,0);
		var model = this.modelFor('product');
		var title =  model.get('name') +" - Giá trọn gói từ Mỹ | Shippy.vn"; 
		document.title = title;
		Ember.$("meta[property='og\\:title']").attr("content", title);
		Ember.$("meta[property='og\\:image']").attr("content", model.get('img').share_img);
		var url = "http://shippy.vn/products/"+model.get('slug');
		Ember.$("meta[property='og\\:url']").attr("content", url);


	}
});


export default route;
