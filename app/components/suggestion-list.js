import Ember from 'ember';

export default Ember.Component.extend({
	didInsertElement(){
		var $ = Ember.$;


	        //Owl Slider v4
					$(".owl-slider-v4-whatsnew").owlCarousel({
							margin: 0,
							responsive: {
								0:{
									items: 1
								},
								992:{
									items: 2
								},
								1200:{
									items: 3
								},
								1300:{
									items: 4
								}
							},
							nav: false,
							dots: true,
							dotsClass: 'owl-pagination',
							dotClass: 'owl-page',
						});
}



});
