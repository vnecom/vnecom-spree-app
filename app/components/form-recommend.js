import Ember from 'ember';
import _ from 'underscore';
export default Ember.Component.extend({
  form: {},
  quote:{},
  error: {status:false,message:""},
  actions: {
    getQuote: function(quote_params){
      var self = this;
      var store = this.get('store');
      /*
       * POST /quote/new
       * return a quote with suggested price
       * @params (product_link,original_price,category_id)
       * .
       */

      var quote = store.createRecord("quote",{
        product_link: quote_params.product_link,
        original_price: quote_params.original_price,
        category_id: quote_params.category_id
      });

      //Save quote and add to its category in backend, frontend's category doesnt need to update
      quote
      .save()
      .then(function(data){
        //NEXT: send this action to quote's main page to process next step which is to show generated quote
        self.sendAction('generateQuote',data.id);
      })
      .catch(function(err){
        window.alert(err);
      });

    },
    submitForm: function(obj){
      console.log(obj)
      function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
      }

      if(!validateEmail(obj.email)){
        this.set("error",{status:true,message:"Vui lòng nhập địa chỉ email hợp lệ."});
      } else if (_.values(obj).length <5){
        this.set("error",{status:true,message:"Vui lòng nhập đủ thông tin:)"});
      } else{
        this.set("error",{status:false,message:"OK"});


        //no error, create an order
        var quote = {product_link:obj.product_link,original_price:obj.original_price};
        var order = this.get("store").createRecord("quote",quote);
        Ember.$("#recSubmit").hide();
        order.save().then(function(data){
          console.log(data);

          Ember.$("#main").hide();
          Ember.$("#success").show();
        }).catch(function(err){
          console.log(err);
          Ember.$("#recSubmit").show();
        });
      }



    }
  },
  didInsertElement(){
    var $ = Ember.$;
    $("#success").hide();
    $("#main").show();
    this.set("form",{});

    //jQuery time
    var current_fs;
    var next_fs;
    var previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches
    $(".next").click(function(){
      if(animating) {
        return false;

      }
      this.error=false;
      animating = true;

      current_fs = $(this).parent();
      next_fs = $(this).parent().next();

      //activate next step on progressbar using the index of next_fs
      $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

      //show the next fieldset
      next_fs.show();
      //hide the current fieldset with style
      current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
          //as the opacity of current_fs reduces to 0 - stored in "now"
          //1. scale current_fs down to 80%
          scale = 1 - (1 - now) * 0.2;
          //2. bring next_fs from the right(50%)
          left = (now * 50)+"%";
          //3. increase opacity of next_fs to 1 as it moves in
          opacity = 1 - now;
          current_fs.css({
            'transform': 'scale('+scale+')',
            'position': 'absolute'
          });
          next_fs.css({'left': left, 'opacity': opacity});
        },
        duration: 800,
        complete: function(){
          current_fs.hide();
          animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
      });
    });

    $(".previous").click(function(){
      if(animating){ return false;}
      animating = true;

      current_fs = $(this).parent();
      previous_fs = $(this).parent().prev();

      //de-activate current step on progressbar
      $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

      //show the previous fieldset
      previous_fs.show();
      //hide the current fieldset with style
      current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
          //as the opacity of current_fs reduces to 0 - stored in "now"
          //1. scale previous_fs from 80% to 100%
          scale = 0.8 + (1 - now) * 0.2;
          //2. take current_fs to the right(50%) - from 0%
          left = ((1-now) * 50)+"%";
          //3. increase opacity of previous_fs to 1 as it moves in
          opacity = 1 - now;
          current_fs.css({'left': left});
          previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
        },
        duration: 800,
        complete: function(){
          current_fs.hide();
          animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
      });
    });


  }
});
